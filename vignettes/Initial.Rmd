---
title: "Analysis of Growth Data with Mortality"
author: "Andrés López-Sepulcre and Anurag Agrawal"
date: "`r Sys.Date()`"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(glmmTMB)
library(tidyverse)
library(ggeffects)
```

### The Motivation

The purpose of this exercise is to better understand how to analyze data on individual growth that has missing cases due to mortality.

This can be done through the use of zero inflated distributions (e.g. zero inflated Normal or Gamma), which separtes the data into the probability of mortality (zeroes) and the growth conditional on survival. The question is:

- How do we obtain a joint estimate of growth that includes the 'no growth' associated with mortality?

- How do we calculate the significance of the factors of interests on the joint distribution (rather than its individual components)?

### The Data

We explore this question using data from Monarch caterpilars *Danaus plexipus* which have been exposed to two treatments: single chemical compounds in the host plant vs. multiple compounds.

We can read and plot the raw data on growth

```{r}
data <- read.csv('../data/cur mix.csv') %>%
  rename(mass = `frozen.cat.mass..mg.`) %>%
  mutate(type = relevel(factor(type), ref = 'single'))

ggplot(data, aes(x = type, y = mass)) +
  geom_jitter(width = 0.15) +
  ylab('Frozen caterpilar mass (mg)') +
  xlab('Treatment type') +
  theme_bw()

```

As expected, there are a good number of zeroes due to death.


### Fitting Zero-inflated Models

If we assume that growth follows a Normal distribution, we can assume a zero-inflated Normal distribution to the data in hand, where a Binomial process governs the probability of death, and the Normal distribution models growth conditional on survival. This can be done using function `glmmTMB` as follows.

```{r}
model <- glmmTMB(mass ~ type,
                 family = gaussian,
                 ziformula = ~ type,
                 data)

summary(model)
```
This tells us that, while there is zero inflation, there is no difference in mortality between two treatment types, but growth of survivors does differ and is larger under the single type.

### Joint Effect on Growth

So far we have only made statements on survivors and dead separately. What if we want to make a summary statement on the whole starting population, considering that the growth of non survivors is zero? We can calculate the predicted growth of both treatment types using library `ggeffects`.

```{r}
pred <- ggpredict(model, terms = 'type')
pred
```

In other words, individuals from the `single` treatment grow `r round(pred$predicted[1] - pred$predicted[2], 2)`mg more than the `mixed` treatments (counting the dead).

Which we can plot.

```{r}
ggplot(pred, aes(y = predicted, x = x)) +
  geom_point(size = 3) +
  geom_errorbar(aes(ymax = conf.high, ymin = conf.low),
                width = 0.2) +
  ylab('Frozen caterpilar mass (mg)') +
  xlab('Treatment type') +
  theme_bw()

```

### Significance of the Joint Distribution

Although we have calculated the predictions for each treatment, we do not yet have an overall significance for growth that integrates both the Binomial and Normal processes that compose it. We can do this by performing likelihood ratio tests of alternative zero-inflated models that incorporate or not the effect of treatment type. We already have the model that incorporates treatment type (`model`), all we need now is a null model that doesn't, but that it is equally zero inflated.

```{r}
model0 <- glmmTMB(mass ~ 1,
                 family = gaussian,
                 ziformula = ~ 1,
                 data)
```

Now we can perform the likelihood ratio test.

```{r}
anova(model, model0)
```
We can now say that growth (regardless of whether the individual dies or lives) is significantly different between the two treatments.



